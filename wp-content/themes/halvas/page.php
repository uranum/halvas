<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package halvas
 */

get_header(); ?>

    <div class="content">
        <div id="featured"></div>
        <div class="Post">
            <div class="Post-body">
                <div class="Post-inner">
                    <div class="PostContent">
	                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                        <?php if (have_posts()) : while (have_posts()) : the_post();?>
						    <?php the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div>
                    <div class="cleared"></div>
                </div>
            </div>
        </div>
    </div>

<?php
get_sidebar();
get_footer();

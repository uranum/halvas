<?php
/**
 * The header for our theme
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package halvas
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!--[if IE 6]>
    <link rel="stylesheet" href="/style.ie6.css" type="text/css"/><![endif]-->
    <!--[if IE 7]>
    <link rel="stylesheet" href="/style.ie7.css" type="text/css" media="screen"/><![endif]-->
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--<div id="page" class="site">-->

<div class="PageBackgroundSimpleGradient"></div>

<div class="PageBackgroundGlare">
    <div class="PageBackgroundGlareImage"></div>
</div>

<div class="Main">
    <div class="Sheet">
        <div class="Sheet-cc"></div>

        <div class="Sheet-body">
            <div class="Header">
                <div class="Header-jpeg"></div>

                <div class="logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>" title="Оборудование для производства халвы"><img
                                src="<?php bloginfo('template_directory') ?>/assets/images/logo.png" alt="Оборудование для производства халвы"/></a>
                </div>
            </div>

            <nav id="site-navigation" class="nav" role="navigation">
                <div class="l"></div>
                <div class="r"></div>
                <ul class="artmenu">
                    <li class="leaf first"><a href="/" title="Главная"><span class="l"></span><span class="r"></span><span class="t">Главная</span></a></li>
                    <li><span class="separator"></span></li>
                    <li class="leaf first"><a href="/company.html" title="О компании"><span class="l"></span><span class="r"></span><span class="t">О компании</span></a></li>
                    <li><span class="separator"></span></li>
                    <li class="leaf"><a href="/kontakty.html" title="Контакты"><span class="l"></span><span class="r"></span><span class="t">Контакты</span></a></li>
                    <li><span class="separator"></span></li>
                    <li class="leaf"><a href="/tseny.html" title="Цены"><span class="l"></span><span class="r"></span><span class="t">Цены</span></a></li>
                </ul>
            </nav>

            <div id="content" class="contentLayout">
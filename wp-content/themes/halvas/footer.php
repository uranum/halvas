<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package halvas
 */

?>

	</div><!-- #content -->

<div class="cleared"></div>


<div class="Footer">
    <div class="Footer-inner">
        <div class="Footer-text">
			<p>© 2002-<?= date('Y') ?> ООО "ЭРА", Все права защищены. При использовании материалов с данного сайта, прямая ссылка на halvas.ru обязательна.
          <!--LiveInternet counter--><script type="text/javascript">
                document.write("<a href='//www.liveinternet.ru/click' "+
                    "target=_blank><img src='//counter.yadro.ru/hit?t14.2;r"+
                    escape(document.referrer)+((typeof(screen)=="undefined")?"":
                        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                    ";h"+escape(document.title.substring(0,150))+";"+Math.random()+
                    "' alt='' title='LiveInternet: показано число просмотров за 24"+
                    " часа, посетителей за 24 часа и за сегодня' "+
                    "border='0' width='88' height='31'><\/a>")
          </script><!--/LiveInternet-->
      </p>
        </div>
    </div>
    <div class="Footer-background"></div>
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

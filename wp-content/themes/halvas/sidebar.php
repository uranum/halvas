<?php
/**
 * The sidebar containing the main widget area
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package halvas
 */

$аргументыПервогоМеню = [
	'menu'            => 'Оборудование',
	'container'       => 'div',
	'menu_class'      => 'Block',
	'menu_id'         => 'equipment',
	'container_class' => 'menu',
	'theme_location'  => 'right_menu_1',
];
$аргументыВторогоМеню = [
	'menu'            => 'Документация',
	'container'       => 'div',
	'menu_class'      => 'Block',
	'menu_id'         => 'documents',
	'container_class' => 'menu',
	'theme_location'  => 'right_menu_2',
];
?>

<div class="sidebar1">
    <div class="Block">
        <div class="Block-tl"></div>
        <div class="Block-tr"></div>
        <div class="Block-bl"></div>
        <div class="Block-br"></div>
        <div class="Block-tc"></div>
        <div class="Block-bc"></div>
        <div class="Block-cl"></div>
        <div class="Block-cr"></div>
        <div class="Block-cc"></div>
        <div class="Block-body">
            <div class="BlockHeader">
                <div class="l"></div>
                <div class="r"></div>
                <div class="header-tag-icon">
                    <div class="t">
                        Оборудование
                    </div>
                </div>
            </div>
            <div class="BlockContent">
                <div class="BlockContent-body">
					<?php wp_nav_menu($аргументыПервогоМеню); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sidebar1">
    <div class="Block">
        <div class="Block-tl"></div>
        <div class="Block-tr"></div>
        <div class="Block-bl"></div>
        <div class="Block-br"></div>
        <div class="Block-tc"></div>
        <div class="Block-bc"></div>
        <div class="Block-cl"></div>
        <div class="Block-cr"></div>
        <div class="Block-cc"></div>
        <div class="Block-body">
            <div class="BlockHeader">
                <div class="l"></div>
                <div class="r"></div>
                <div class="header-tag-icon">
                    <div class="t">
                        Документация
                    </div>
                </div>
            </div>
            <div class="BlockContent">
                <div class="BlockContent-body">
					<?php wp_nav_menu($аргументыВторогоМеню); ?>
                </div>
            </div>
        </div>
    </div>
</div>

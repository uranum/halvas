<?php
/*
  Plugin Name: Trash Duplicate And 301 Redirect
  Plugin URI: https://wordpress.org/plugins/trash-duplicate-and-301-redirect/
  Description: Find and delete duplicates posts, custom posts and pages specifying which one to keep (newest or oldest) and 301 redirection to the post you are keeping.
  Author: Solwin Infotech
  Author URI: https://www.solwininfotech.com/
  Copyright: Solwin Infotech
  Version: 1.3.4
  Requires at least: 4.0
  Tested up to: 4.9
  
  Text Domain: trash-duplicate-and-301-redirect
  Domain Path: /languages/
 */

/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}

define('TDRD_PLUGIN_URL', plugin_dir_url(__FILE__));
define('TDRD_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once TDRD_PLUGIN_DIR . 'includes/promo_notice.php';

/**
 * This function is call when initializing this plugin
 */
if (!function_exists('TDRD_init')) {

    function TDRD_init() {
        // set the current plugin version
        $trash_duplicates_version = '1.3.4';
        $trash_duplicates_options = get_option('trash_duplicates_options');
        // if it's not the latest version.
        if (version_compare($trash_duplicates_version, $trash_duplicates_options['version'], '>')) {
            $trash_duplicates_options['version'] = $trash_duplicates_version;
            update_option('trash_duplicates_options', $trash_duplicates_options);
        }
        //  Load admin scripts
        if (is_admin()) {
            require_once( 'trash-duplicates-admin.php' );
            require_once( 'redirect_admin.php' );
        } else {
            require_once( 'redirect_client.php' );
        }
    }

}
add_action('init', 'TDRD_init', 0);

/**
 * plugin text domain
 */
add_action('plugins_loaded', 'load_text_domain_trash_duplicate_301_redirect');
if (!function_exists('load_text_domain_trash_duplicate_301_redirect')) {

    function load_text_domain_trash_duplicate_301_redirect() {
        load_plugin_textdomain('trash-duplicate-and-301-redirect', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }

}

/**
 * admin scripts
 */
if (!function_exists('tdr_admin_scripts')) {

    function tdr_admin_scripts() {
        $screen = get_current_screen();
        $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/trash-duplicate-and-301-redirect/trash-duplicates.php', $markup = true, $translate = true);
        $current_version = $plugin_data['Version'];
        $old_version = get_option('tdr_version');
        if ($old_version != $current_version) {
            update_option('is_user_subscribed_cancled', '');
            update_option('tdr_version', $current_version);
        }
        if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes') {
            wp_enqueue_script('thickbox');
            wp_enqueue_style('thickbox');
            wp_register_script('custom_wp_admin_js', plugins_url('js/admin_script.js', __FILE__));
            wp_enqueue_script('custom_wp_admin_js');
        }
    }

}
add_action('admin_enqueue_scripts', 'tdr_admin_scripts');
add_action('plugins_loaded', 'latest_news_solwin_feed');
add_action('current_screen', 'tdrd_footer');
add_filter('set-screen-option', 'TDRD_set_screen_option', 10, 3);

/**
 * Add solwin news dashboard
 */
if (!function_exists('latest_news_solwin_feed')) {

    function latest_news_solwin_feed() {
        // Register the new dashboard widget with the 'wp_dashboard_setup' action
        add_action('wp_dashboard_setup', 'solwin_latest_news_with_product_details');
        if (!function_exists('solwin_latest_news_with_product_details')) {

            function solwin_latest_news_with_product_details() {
                add_screen_option('layout_columns', array('max' => 3, 'default' => 2));
                add_meta_box('trash-duplicates_dashboard_widget', __('News From Solwin Infotech', 'trash-duplicate-and-301-redirect'), 'solwin_dashboard_widget_news', 'dashboard', 'normal', 'high');
            }

        }
        if (!function_exists('solwin_dashboard_widget_news')) {

            function solwin_dashboard_widget_news() {
                echo '<div class="rss-widget">'
                . '<div class="solwin-news"><p><strong>Solwin Infotech News</strong></p>';
                wp_widget_rss_output(array(
                    'url' => 'https://www.solwininfotech.com/feed/',
                    'title' => __('News From Solwin Infotech', 'trash-duplicate-and-301-redirect'),
                    'items' => 5,
                    'show_summary' => 0,
                    'show_author' => 0,
                    'show_date' => 1
                ));
                echo '</div>';
                $title = $link = $thumbnail = "";
                //get Latest product detail from xml file
                $file = 'https://www.solwininfotech.com/documents/assets/latest_product.xml';
                echo '<div class="display-product">'
                . '<div class="product-detail"><p><strong>' . __('Latest Product', 'trash-duplicate-and-301-redirect') . '</strong></p>';
                $response = wp_remote_post($file);
                if (is_wp_error($response)) {
                    $error_message = $response->get_error_message();
                    echo "<p>" . __('Something went wrong', 'trash-duplicate-and-301-redirect') . " : $error_message" . "</p>";
                } else {
                    $body = wp_remote_retrieve_body($response);
                    $xml = simplexml_load_string($body);
                    $title = $xml->item->name;
                    $thumbnail = $xml->item->img;
                    $link = $xml->item->link;
                    $allProducttext = $xml->item->viewalltext;
                    $allProductlink = $xml->item->viewalllink;
                    $moretext = $xml->item->moretext;
                    $needsupporttext = $xml->item->needsupporttext;
                    $needsupportlink = $xml->item->needsupportlink;
                    $customservicetext = $xml->item->customservicetext;
                    $customservicelink = $xml->item->customservicelink;
                    $joinproductclubtext = $xml->item->joinproductclubtext;
                    $joinproductclublink = $xml->item->joinproductclublink;
                    echo '<div class="product-name"><a href="' . $link . '" target="_blank">'
                    . '<img alt="' . $title . '" src="' . $thumbnail . '"> </a>'
                    . '<a href="' . $link . '" target="_blank">' . $title . '</a>'
                    . '<p><a href="' . $allProductlink . '" target="_blank" class="button button-default">' . $allProducttext . ' &RightArrow;</a></p>'
                    . '<hr>'
                    . '<p><strong>' . $moretext . '</strong></p>'
                    . '<ul>'
                    . '<li><a href="' . $needsupportlink . '" target="_blank">' . $needsupporttext . '</a></li>'
                    . '<li><a href="' . $customservicelink . '" target="_blank">' . $customservicetext . '</a></li>'
                    . '<li><a href="' . $joinproductclublink . '" target="_blank">' . $joinproductclubtext . '</a></li>'
                    . '</ul>'
                    . '</div>';
                }
                echo '</div></div><div class="clear"></div>'
                . '</div>';
            }

        }
    }

}

/**
 * Add Footer link
 */
if (!function_exists('tdrd_footer')) {

    function tdrd_footer() {
        $screen = get_current_screen();
        if ((isset($_GET['page']) && ($_GET['page'] == 'trash_duplicates' || $_GET['page'] == '301_redirects' || $_GET['page'] == 'about_trash_duplicates'))) {
            add_filter('admin_footer_text', 'tdrd_remove_footer_admin'); //change admin footer text
        }
    }

}

/**
 * Add rating html at footer of admin
 * @return html rating
 */
if (!function_exists('tdrd_remove_footer_admin')) {

    function tdrd_remove_footer_admin() {
        ob_start();
        ?>
        <p id="footer-left" class="alignleft">
            <?php _e('If you like ', 'trash-duplicate-and-301-redirect'); ?>
            <a href="https://www.solwininfotech.com/product/wordpress-plugins/trash-duplicate-and-301-redirect/" target="_blank"><strong><?php _e('Trash Duplicate And 301 Redirection Plugin', 'trash-duplicate-and-301-redirect'); ?></strong></a>
            <?php _e('please leave us a', 'trash-duplicate-and-301-redirect'); ?>
            <a class="bdp-rating-link" data-rated="Thanks :)" target="_blank" href="https://wordpress.org/support/plugin/trash-duplicate-and-301-redirect/reviews?filter=5#new-post">&#x2605;&#x2605;&#x2605;&#x2605;&#x2605;</a>
            <?php _e('rating. A huge thank you from Solwin Infotech in advance!', 'trash-duplicate-and-301-redirect'); ?>
        </p>
        <?php
        return ob_get_clean();
    }

}

/**
 * start session if not
 */
if (!function_exists('tdr_session_start')) {

    function tdr_session_start() {
        if (version_compare(phpversion(), "5.4.0") != -1) {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
        } else {
            if (session_id() == '') {
                session_start();
            }
        }
    }

}
add_action('init', 'tdr_session_start');

/**
 * subscribe email form
 */
if (!function_exists('tdr_subscribe_mail')) {

    function tdr_subscribe_mail() {
        $customer_email = get_option('admin_email');
        $current_user = wp_get_current_user();
        $f_name = $current_user->user_firstname;
        $l_name = $current_user->user_lastname;
        if (isset($_POST['sbtEmail'])) {
            $_SESSION['success_msg'] = 'Thank you for your subscription.';
            //Email To Admin
            update_option('is_user_subscribed', 'yes');
            $customer_email = trim($_POST['txtEmail']);
            $customer_name = trim($_POST['txtName']);
            $to = 'plugins@solwininfotech.com';
            $from = get_option('admin_email');
            $headers = "MIME-Version: 1.0;\r\n";
            $headers .= "From: " . strip_tags($from) . "\r\n";
            $headers .= "Content-Type: text/html; charset: utf-8;\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
            $subject = 'New user subscribed from Plugin - Trash Duplicate And 301 Redirection';
            $body = '';
            ob_start();
            ?>
            <div style="background: #F5F5F5; border-width: 1px; border-style: solid; padding-bottom: 20px; margin: 0px auto; width: 750px; height: auto; border-radius: 3px 3px 3px 3px; border-color: #5C5C5C;">
                <div style="border: #FFF 1px solid; background-color: #ffffff !important; margin: 20px 20px 0;
                     height: auto; -moz-border-radius: 3px; padding-top: 15px;">
                    <div style="padding: 20px 20px 20px 20px; font-family: Arial, Helvetica, sans-serif;
                         height: auto; color: #333333; font-size: 13px;">
                        <div style="width: 100%;">
                            <strong>Dear Admin (Trash Duplicate And 301 Redirection plugin developer)</strong>,
                            <br />
                            <br />
                            Thank you for developing useful plugin.
                            <br />
                            <br />
                            I <?php echo $customer_name; ?> want to notify you that I have installed plugin on my <a href="<?php echo home_url(); ?>">website</a>. Also I want to subscribe to your newsletter, and I do allow you to enroll me to your free newsletter subscription to get update with new products, news, offers and updates.
                            <br />
                            <br />
                            I hope this will motivate you to develop more good plugins and expecting good support form your side.
                            <br />
                            <br />
                            Following is details for newsletter subscription.
                            <br />
                            <br />
                            <div>
                                <table border='0' cellpadding='5' cellspacing='0' style="font-family: Arial, Helvetica, sans-serif; font-size: 13px;color: #333333;width: 100%;">
                                    <?php if ($customer_name != '') {
                                        ?>
                                        <tr style="border-bottom: 1px solid #eee;">
                                            <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                                Name<span style="float:right">:</span>
                                            </th>
                                            <td style="padding: 8px 5px;">
                                                <?php echo $customer_name; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <tr style="border-bottom: 1px solid #eee;">
                                            <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                                Name<span style="float:right">:</span>
                                            </th>
                                            <td style="padding: 8px 5px;">
                                                <?php echo home_url(); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                            Email<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo $customer_email; ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                            Website<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo home_url(); ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left; width: 120px;">
                                            Date<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo date('d-M-Y  h:i  A'); ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left; width: 120px;">
                                            Plugin<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo 'Trash Duplicate And 301 Redirection'; ?>
                                        </td>
                                    </tr>
                                </table>
                                <br /><br />
                                Again Thanks you
                                <br />
                                <br />
                                Regards
                                <br />
                                <?php echo $customer_name; ?>
                                <br />
                                <?php echo home_url(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $body = ob_get_clean();
            wp_mail($to, $subject, $body, $headers);
        }
        if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes') {
            ?>
            <div id="subscribe_widget_trash" style="display:none;">
                <div class="subscribe_widget">
                    <h3>Notify to plugin developer and subscribe.</h3>
                    <form class='sub_form' name="frmSubscribe" method="post" action="<?php echo admin_url() . 'admin.php?page=trash_duplicates'; ?>">
                        <div class="sub_row"><label>Your Name: </label><input placeholder="Your Name" name="txtName" type="text" value="<?php echo $f_name . ' ' . $l_name; ?>" /></div>
                        <div class="sub_row"><label>Email Address: </label><input placeholder="Email Address" required name="txtEmail" type="email" value="<?php echo $customer_email; ?>" /></div>
                        <input class="button button-primary" type="submit" name="sbtEmail" value="Notify & Subscribe" />
                    </form>
                </div>
            </div>
            <?php
        }
        if (isset($_GET['page'])) {
            if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes' && ($_GET['page'] == 'trash_duplicates' || $_GET['page'] == '301_redirects')) {
                ?>
                <a style="display:none" href="#TB_inline?max-width=400&height=210&inlineId=subscribe_widget_trash" class="thickbox" id="subscribe_thickbox"></a>
                <?php
            }
        }
    }

}
add_action('admin_head', 'tdr_subscribe_mail', 10);

/**
 * user cancel subscribe
 */
if (!function_exists('wp_ajax_tdr_close_tab')) {

    function wp_ajax_tdr_close_tab() {
        update_option('is_user_subscribed_cancled', 'yes');
        exit();
    }

}
add_action('wp_ajax_close_tab', 'wp_ajax_tdr_close_tab');

if (!function_exists('TDRD_advertisement_sidebar')) {

    function TDRD_advertisement_sidebar() {
        ?>
        <div class="td-admin-sidebar">
            <div class="td-help">
                <h2><?php _e('Help to improve this plugin!', 'trash-duplicate-and-301-redirect'); ?></h2>
                <div class="help-wrapper">
                    <span><?php _e('Enjoyed this plugin?', 'trash-duplicate-and-301-redirect'); ?></span>
                    <span><?php _e(' You can help by', 'trash-duplicate-and-301-redirect'); ?>
                        <a href="https://wordpress.org/support/plugin/trash-duplicate-and-301-redirect/reviews?filter=5#new-post" target="_blank">
                            <?php _e(' rating this plugin on wordpress.org', 'trash-duplicate-and-301-redirect'); ?>
                        </a>
                    </span>
                    <div class="td-total-download">
                        <?php _e('Downloads:', 'trash-duplicate-and-301-redirect'); ?><?php get_trash_duplicates_total_downloads(); ?>
                        <?php
                        $wp_version = get_bloginfo('version');
                        if ($wp_version > 3.8) {
                            wp_trash_duplicates_star_rating();
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="useful_plugins">
                <h2><?php _e('Trash Duplicate And 301 Redirection PRO', 'trash-duplicate-and-301-redirect'); ?></h2>
                <div class="help-wrapper">
                    <div class="pro-content">
                        <ul class="advertisementContent">
                            <li><?php _e("Enable/disable redirection", 'trash-duplicate-and-301-redirect') ?></li>
                            <li><?php _e("Trash without 301 redirection", 'trash-duplicate-and-301-redirect') ?></li>
                            <li><?php _e("301 redirection without Trash", 'trash-duplicate-and-301-redirect') ?></li>
                            <li><?php _e("Wildcard redirection feature", 'trash-duplicate-and-301-redirect') ?></li>
                            <li><?php _e("Delete Post Meta Permanently", 'trash-duplicate-and-301-redirect') ?></li>
                            <li><?php _e("Import/Export Redirections", 'trash-duplicate-and-301-redirect') ?></li>
                        </ul>
                        <p class="pricing_change"><?php _e("Now only at", 'trash-duplicate-and-301-redirect') ?> <ins>$24</ins></p>
                    </div>
                    <div class="pre-book-pro">
                        <a href="https://codecanyon.net/item/trash-duplicate-and-301-redirect-pro-for-wordpress/20885697?ref=solwin" target="_blank">
                            <?php _e('Buy Now on Codecanyon', 'trash-duplicate-and-301-redirect'); ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="td-support">
                <h3><?php _e('Need Support?', 'trash-duplicate-and-301-redirect'); ?></h3>
                <div class="help-wrapper">
                    <span><?php _e('Check out the', 'trash-duplicate-and-301-redirect') ?>
                        <a href="https://wordpress.org/plugins/trash-duplicate-and-301-redirect/faq/" target="_blank"><?php _e('FAQs', 'trash-duplicate-and-301-redirect'); ?></a>
                        <?php _e('and', 'trash-duplicate-and-301-redirect') ?>
                        <a href="https://wordpress.org/support/plugin/trash-duplicate-and-301-redirect" target="_blank"><?php _e('Support Forums', 'trash-duplicate-and-301-redirect') ?></a>
                    </span>
                </div>
            </div>
        </div><?php
    }

}

if (!function_exists('TDRD_plugin_links')) {

    function TDRD_plugin_links($links) {
        $links[] = '<a target="_blank" href="' . esc_url('https://www.solwininfotech.com/documents/wordpress/trash-duplicate-and-301-redirect/') . '">' . __('Documentation', 'trash-duplicate-and-301-redirect') . '</a>';
        return $links;
    }

}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'TDRD_plugin_links');

// Deactivate trash duplicate pro plugin when trash duplicate lite is activate
register_activation_hook(__FILE__, 'TDRD_plugin_deactivate');

if (!function_exists('TDRD_plugin_deactivate')) {

    function TDRD_plugin_deactivate() {
        if (is_plugin_active('trash-duplicate-and-301-redirect-pro/trash-duplicates.php')) {
            deactivate_plugins('trash-duplicate-and-301-redirect-pro/trash-duplicates.php');
        }
    }

}

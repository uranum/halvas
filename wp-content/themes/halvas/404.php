<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package halvas
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Кажется, этой страницы уже (или еще) не существует!', 'halvas' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'Попробуйте поискать на сайте', 'halvas' ); ?></p>

					<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( halvas_categorized_blog() ) :
					?>

					<?php endif; ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->
            
            <div style="height: 500px;"></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
